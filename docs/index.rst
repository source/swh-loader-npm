.. _swh-loader-npm:

Software Heritage - npm loader
==============================

Loader for `npm <https://wwww.npmjs.com/>`_ packages.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   /apidoc/swh.loader.npm
